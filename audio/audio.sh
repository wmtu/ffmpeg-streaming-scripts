#!/bin/bash

# general settings
alert_mail=""
status_mail=""
slack_mail=""

# rtp audio source settings
rtp_source="239.192.0.5" # this is a multicast IP used by axia
rtp_channel="5004"

# icecast settings
# name of the stream
ice_name="WMTU 91.9FM | Houghton, MI"
# description of the stream
ice_desc="Michigan Tech's Campus Radio Station"
# full url of the stream (should be the actualy stream url)
ice_url="https://stream.wmtu.fm/wmtu-live"
# music genre
ice_genre="College Radio"
# icecast broadcasting source username (usually source)
ice_user="source"
# icecast broadcaster password
ice_pass=""
# icecast stream server
ice_server="localhost:8000"
# icecast streaming endpoint
ice_hq_endpoint="wmtu-live"
ice_sd_endpoint="wmtu-192k"

echo $$ > /run/stream/wmtu-audio.run

printf "Icecast Live Stream is UP" | /usr/bin/mail -s "Icecast Live Stream is UP" $slack_mail

# mp3 streams
dumprtp $rtp_source $rtp_channel | ffmpeg -hide_banner -f s24be -ar 48k -ac 2 -i - \
    -acodec libmp3lame -f mp3 -b:a 320K -content_type "audio/mpeg" \
    -ice_name "$ice_name" -ice_description "$ice_desc" -ice_url "$ice_url" -ice_genre "$ice_genre" -password "$ice_pass" icecast://$ice_user@$ice_server/$ice_hq_endpoint\
    -acodec libmp3lame -f mp3 -b:a 192K -content_type "audio/mpeg" \
    -ice_name "$ice_name" -ice_description "$ice_desc" -ice_url "$ice_url" -ice_genre "$ice_genre" -password "$ice_pass" icecast://$ice_user@$ice_server/$ice_sd_endpoint

printf "Icecast Live Stream is DOWN" | /usr/bin/mail -s "Icecast Live Stream is DOWN" $slack_mail

rm /run/stream/wmtu-audio.run

sleep 60

exit
