#!/bin/bash

debug=0

# check it the audio process is running
# check for the PID file existance
# if it doesn't exist then run our audio stream script
# if it does exist then check if the PID is real, and if not run it

# wmtu-live
if [ ! -e /run/stream/wmtu-audio.run ]
then
    if [ $debug -eq 1 ]; then echo "Launching wmtu-audio"; fi
    ip route replace 224.0.0.0/4 dev ens160
    screen -S wmtu-audio -dma /bin/bash /opt/stream/audio/audio.sh
else
    audio_pid=`cat /run/stream/wmtu-audio.run`
    if ! ps -p $audio_pid > /dev/null; then
        if [ $debug -eq 1 ]; then echo "Launching wmtu-audio"; fi
        ip route replace 224.0.0.0/4 dev ens160
        rm /run/stream/wmtu-audio.run
        screen -S wmtu-audio -dma /bin/bash /opt/stream/audio/audio.sh
    else
        if [ $debug -eq 1 ]; then echo "WMTU audio already running: $audio_pid"; fi
    fi
fi

exit
