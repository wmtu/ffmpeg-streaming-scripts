#!/bin/bash

# general settings
alert_mail=""
status_mail=""
slack_mail=""

# ffmpeg settings
audio_source="anullsrc" # youtube doesn't like copyrighted audio :(
video_source=""
# length of time to stream in format <hr>:<min>:<sec>
stream_duration="6:00:00"
# youtube streamer url with key
yt_url="rtmp://a.rtmp.youtube.com/live2/key"

echo $$ > /run/stream/youtube.run

printf "YouTube Live Stream is UP" | /usr/bin/mail -s "YouTube Live Stream is UP" $slack_mail

# stream to youtube
ffmpeg -hide_banner -nostats -v 32 -thread_queue_size 2048 -f lavfi -i $audio_source -i $video_source \
	-vcodec copy -acodec aac -b:a 128k -ar 44100 -crf 18 -preset veryfast -t $stream_duration -f flv $yt_url

printf "YouTube Live Stream is DOWN" | /usr/bin/mail -s "YouTube Live Stream is DOWN" $slack_mail

sleep 60

rm /run/stream/youtube.run

exit
