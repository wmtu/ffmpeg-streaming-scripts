#!/bin/bash

# start the wmtu video streams
screen -S youtube -dma bash /opt/stream/youtube.sh
screen -S twitch -dma bash /opt/stream/twitch.sh
screen -S facebook -dma bash /opt/stream/facebook.sh

exit
