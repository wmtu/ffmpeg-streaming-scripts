#!/bin/bash

# general settings
alert_mail=""
status_mail=""
slack_mail=""

# rtp audio source settings
rtp_source="239.192.0.5" # this is a multicast IP used by axia
rtp_channel="5004"

# ffmpeg settings
audio_source="-"
video_source=""
# length of time to stream in format <hr>:<min>:<sec>
stream_duration="2:00:00"
# twitch streamer url with key
twitch_url="rtmp://live.twitch.tv/app/live_"

echo $$ > /run/stream/twitch.run

printf "Twitch Live Stream is UP" | /usr/bin/mail -s "Twitch.tv Live Stream is UP" $slack_mail

# stream the booth webcam to video sources
# we pull audio from the rtp stream similar to the icecast stream
# audio needs to be delayed due to the video feed being delayed
dumprtp $rtp_source $rtp_channel | ffmpeg -hide_banner -nostats -v 32 -thread_queue_size 2048 \
	-f s24be -ar 48k -ac 2 -i -itsoffset 00:00:05 $audio_source -f lavfi -i anullsrc -i $video_source \
	-vcodec copy -acodec libmp3lame -f mp3 -q:a 0 -content_type "audio/mpeg" \
	-flags +global_header -crf 18 -preset veryfast -t $stream_duration -f flv $twitch_url

printf "Twitch Live Stream is DOWN" | /usr/bin/mail -s "Twitch.tv Live Stream is DOWN" $slack_mail

sleep 60

rm /run/stream/twitch.run

exit
