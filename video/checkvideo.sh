#!/bin/bash

debug=0

# check it the video process is running
# check for the PID file existance
# if it doesn't exist then run our video stream script
# if it does exist then check if the PID is real, and if not run it

# twitch
if [ ! -e /run/stream/twitch.run ]
then
    if [ $debug -eq 1 ]; then echo "Launching twitch"; fi
    screen -S twitch -dma /bin/bash /opt/stream/video/twitch.sh
else
    Twitch_PID=`cat /run/stream/twitch.run`
    if ! ps -p $Twitch_PID > /dev/null; then
        if [ $debug -eq 1 ]; then echo "Launching Twitch"; fi
        rm /run/stream/twitch.run
        screen -S twitch -dma /bin/bash /opt/stream/video/twitch.sh
    else
        if [ $debug -eq 1 ]; then echo "Twitch already running:  $Twitch_PID"; fi
    fi
fi


# youtube
if [ ! -e /run/stream/youtube.run ]
then
    if [ $debug -eq 1 ]; then echo "Launching YouTube"; fi
    screen -S youtube -dma /bin/bash /opt/stream/video/youtube.sh
else
    YT_PID=`cat /run/stream/youtube.run`
    if ! ps -p $YT_PID > /dev/null; then
        if [ $debug -eq 1 ]; then echo "Launching YouTube"; fi
        rm /run/stream/youtube.run
        screen -S youtube -dma /bin/bash /opt/stream/video/youtube.sh
    else
        if [ $debug -eq 1 ]; then echo "YouTube already running: $YT_PID"; fi
    fi
fi

exit
